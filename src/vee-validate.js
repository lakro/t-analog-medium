import { extend } from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';
 
extend('required', {
  ...required,
  message: 'Поле обязательно для заполнения'
});

extend('email', {
  ...email,
  message: 'Не действительный электронный адрес'
});

extend('failed', {
  ...email,
  message: 'Данные является недействительными'
});
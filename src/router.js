
import VueRouter from 'vue-router';

import  Auth from './components/Auth';
import  Board  from './components/Board';
import FormPost from './components/FormPost';

export default new VueRouter({
    routes: [
        { path: '/auth', component: Auth },
        { path: '/board', component: Board },
        { path: '/post/:id', component: FormPost}
    ]
})
import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';
import api from './config';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {
            login: 'Аноним'
        },
        posts: [],
        post: {
            title: '',
            description: '',
            userId: null,
            createdAt: '',
            updateAt: '',
            claps: 0
        },
        form: {
            login: '',
            password: ''
        },
        pagination: {
            total: null,
            current: 1,
            limit: 10,
            page: 1
        }
    },
    getters: {
        user(state) {
            return state.user;
        },
        posts(state) {
            return state.posts;
        },
        post(state) {
            return state.post;
        },
        form(state) {
            return state.form;
        },
        pagination(state) {
            return state.pagination;
        }
    },
    actions: {
        getPosts(ctx, data) {
            axios.get(`${api.jsonServer}/posts?_page=${data.page}&_limit=${data.limit}`).then(response => {
                ctx.commit('updatePosts', response.data);
                ctx.commit('setPagination', {
                    ...data,
                    total: response.headers['x-total-count'],
                })
            });
        },
        updatePost(ctx, post) {
            axios.put(`${api.jsonServer}/posts/${post.id}`, {
                title: post.title,
                description: post.description,
                claps: post.flag ? post.claps : Number(post.claps) + 1,
                createdAt: post.createdAt,
                updateAt: post.flag ? new Date() : post.updateAt,
                userId: post.userId,
            }, {
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            ).then(response => {
                ctx.commit('updateClaps', response.data);
                return true;
            })
        },

        getPost(ctx, data) {
            axios.get(`${api.jsonServer}/posts/${data.id}`).then(response => {
                ctx.commit('getPost', response.data);
            })
        },

        createPost(ctx, data) {
            axios.post(`${api.jsonServer}/posts`, {
                title: data.title,
                description: data.description,
                claps: 0,
                createdAt: new Date(),
                updateAt: new Date(),
                userId: data.userId,
            }).then(response => {
                ctx.commit('createPost', response.data);
                return true;
            })
        },

        deletePost(ctx, post) {
            axios.delete(`${api.jsonServer}/posts/${post.id}`)
                .then(() => ctx.commit('deletePost', post))
        }

    },
    mutations: {
        setAuth(state, user) {
            state.user = user;
        },

        updatePosts(state, posts) {
            state.posts = posts
        },

        updateClaps(state, changePost) {
            state.posts = state.posts.map(post => {
                if (post.id === changePost.id) {
                    post.claps = changePost.claps;
                }
                return post;
            })
        },
        getPost(state, post) {
            state.post = post;
         },

        createPost(state, createPost) {
           state.posts.push(createPost);
        },

        deletePost(state, deletePost) {
            state.posts = state.posts.filter(post => {
                if (post.id !== deletePost.id) {
                    return post;
                }
            })
        },
        clearForm(state, form) {
            state.form = form;
        },

        setPagination(state, pagination) {
            state.pagination = pagination;
        }


    }
})